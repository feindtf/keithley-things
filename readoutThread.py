# H. Wennlöf, 2018

import threading
import time

import matplotlib.pyplot as plt
import numpy as np
import matplotlib.animation as animation


class readoutThread(threading.Thread):

    def __init__(self, psu, logFileName, startTime):

        threading.Thread.__init__(self)
        
        self.kiethley = psu
        self.logFile = logFileName
        self.startTime = startTime

        self.fig = plt.figure()
        self.ax1 = self.fig.add_subplot(1, 1, 1)

        self.times = [0.0]
        self.currents = [0.0]

        self.running = True

        #Animation. Calls the function self.plotStuff, once every interval.
        ani = animation.FuncAnimation(self.fig, self.plotStuff, interval = 1000)
        
        
        plt.show(block=False)
        #Sadly, plotting has to be done from main thread... TkInter-stuff.
        #This does that, but it's blocked until main thread is done with its stuff.
        
    def run(self):        
        
        while True:
            if self.running:
                #print(self.kiethley.get_current()[0])

                currentTime = time.time() - self.startTime
                current = self.kiethley.get_current()
                
                #Writes current to file.
                #Current, error, timestamp
                with open(self.logFile, "a") as fil:
                    data = str(current[0]) + "," + str(current[1]) + "," + str(currentTime) + "\n"
                    fil.write(data)

                self.times.append(currentTime)
                self.currents.append(current[0])
                
            else:
                continue


    def pauseAcq(self):
        self.running = False

    def restartAcq(self):
        self.running = True


    #Plots the contents of the lists self.times and self.currents
    #Gets called several times, separated by interval
    def plotStuff(self, interval):

        displayTime = 60

        #Want to set axes to include... the last minute?
        #displayTime seconds before the current time
        currentTime = self.times[-1]
        self.times = [time for time in self.times if time > currentTime-displayTime]
        self.currents = self.currents[(len(self.currents)-len(self.times)):]
        
        
        self.ax1.clear()
        
        self.ax1.set_xlabel("Time since start [s]")
        self.ax1.set_ylabel("Current [uA]")
        
        self.ax1.plot(self.times, self.currents)
        return
