# H. Wennlöf, 2018
# F. Feindt,  2021


import yaml
import time
import sys
import matplotlib.pyplot as plt

from Keithley import KeithleySMU2400Series


#############################################
# Simple program to set and monitore voltages
#
# Note:
# Before any settings are changed, the data acquisition needs to be paused
# (as we can't have two processes writing to the # Keithley at the same time).
# Pausing can be done by calling pauseAcq(), unpausing by calling resumeAcq().


# open configuration file
confFileName = sys.argv[1]
print(confFileName)
configuration_file = ''
with open(confFileName, 'r') as file:
    configuration_file = yaml.load(file, Loader=yaml.SafeLoader)

    
# connect to Keithley
vsource = KeithleySMU2400Series(configuration_file)
print("Keithley Initialised.\n")


# enter voltage
#svolt = input('Enter voltage [V]:')
#ivolt = int(svolt)
#vsource.disable_output()
#vsource.set_voltage(ivolt, unit='V')
#vsource.enable_output()


# voltage from config file
V_Start = configuration_file["SetVoltage"]["V_Start"]
V_Step = configuration_file["SetVoltage"]["V_Step"]
V_Set = configuration_file["SetVoltage"]["V_Set"]
try:
    # set V_Start
    vsource.disable_output()
    vsource.set_voltage(V_Start, unit='V')
    vsource.enable_output()
    # ramp to V_Set
    vsource.vramp(V_Set, V_Step, 'V')
except ValueError:
    exit()

# build outfile name
logname = configuration_file["OutfileName"]
startTime = time.time()
logFileName = logname + str(startTime) + ".log"
log = open(logFileName, "a")
log.write( "time_since_start[s] current_value[ua] current_error[ua]\n" )

# setup plot
plt.xlabel('time [s]')
plt.ylabel('current [uA]')
#plt(figsize=(8, 6), dpi=80)

# measure loop
while True:
    try: 
        current = vsource.get_current()
        runtime = time.time() - startTime
        print( "  Running for " + str(runtime) + "s measured current " + str(current[0]) + " uA, voltage: " + str(vsource.get_voltage()[0]))
        log.write( str(runtime) + " " + str(current[0]) + " " + str(current[1]) + "\n" )
        plt.scatter( runtime, current[0] )
        plt.pause(1)
    except KeyboardInterrupt:
        vsource.vramp(V_Start, V_Step, 'V')
        log.close()
        break
