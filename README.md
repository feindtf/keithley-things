# Keithley things

Some files to interact with a Keithley current source (specifically tested on the 2410)

## Details

Files origianlly made to plot current vs time (using `keithleyStuff.py`), but the file `Keithley.py` contains a sort of wrapper for the Keithley interface.
There are functions to set and get currents and voltages, ramping the voltage up, and turning output on and off.

`yaml` is used for setting the configuration. `config_keithley_tj.yaml` is an example configuration file. This is where the COM port is set, so a good place to check if nothing works.
Also where the baud rate is set. Here *and* on the Keithley, and they need to match. Make also sure to set parity to 'EVEN' and terminator to <CR+LF> on the Keithley.

Past Me was rather thorough it seems, so there is even a document with brief instructions for getting things running; `Keithley_program.pdf`.
