from peary import PearyClient
import numpy as np
from Keithley import KeithleySMU2400Series
import yaml
import time
import sys
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
from random import randrange
from datetime import datetime



def main(host='localhost'):
    with PearyClient(host=host) as client:


        client.clear_devices()

        device = client.add_device('dSiPM', '../config/dSiPM_Config_Example.cfg')

        #print("DEVICES")
        #print(client.list_devices())

        # common device functionality
        #print("REGISTERS")
        #print(device.list_registers())

        device.power_on()

        device.configure()




        # open configuration file
        confFileName = "config_keithley__IV_dSiPM.yaml"
        print(confFileName)
        configuration_file = ''
        with open(confFileName, 'r') as file:
            configuration_file = yaml.load(file, Loader=yaml.SafeLoader)

        # connect to Keithley
        vsource = KeithleySMU2400Series(configuration_file)
        print("Keithley Initialised.\n")

        # build outfile name
        logname = configuration_file["OutfileName"]
        startTime = time.time()
        logFileName = logname + str(startTime) + ".log"
        log = open(logFileName, "a")
        #log.write( "time_since_start[s] voltage [V] current_value[ua] current_error[ua]\n" )


        # voltage from config file
        vstart = configuration_file["SetVoltage"]["V_Start"]
        vstep  = configuration_file["SetVoltage"]["V_Step"]
        vset   = configuration_file["SetVoltage"]["V_Set"]
        twait  = configuration_file["SetVoltage"]["t_Wait"]
        t_Wait_1st = configuration_file["SetVoltage"]["t_Wait_1st"]
        n_Points = configuration_file["SetVoltage"]["n_Points"]
        n_Frames = configuration_file["dSiPM"]["n_Frames"]
        Frames_Filename = configuration_file["dSiPM"]["Frames_Filename"]
        IV_Filename = configuration_file["dSiPM"]["IV_Filename"]

        averageVoltage = []
        averageCurrent = []

        try:
            # set V_Start
            vsource.disable_output()
            vsource.set_voltage(vstart, unit='V')
            vsource.enable_output()

        except ValueError:
            exit()



        # take first measurement
        print( "  Waiting " + str(t_Wait_1st) + "s after 1st Voltage")
        time.sleep(t_Wait_1st)
        d = vsource.get_voltage()[0]
        current = vsource.get_current()
        runtime = time.time() - startTime
        print( "  Running for " + str('{:6.2f}'.format(runtime)) + "s, voltage " + str('{:6.2f}'.format(d)) + " V, measured current " + str('{:.5f}'.format(current[0]))  + " uA ")
        #log.write( str('{:6.2f}'.format(runtime)) + " " + str(d) + " " + str(current[0]) + " " + str(current[1]) + "\n" )

        #plot
        fig = plt.figure(figsize=(6, 3))
        xp: float = []
        yp: float = []

        #outfile
        f = open(IV_Filename, "a")

        now = datetime.now()
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

        f.write('Current Time = ' + dt_string + '\n')
        f.write( 'time' + '\t' + 'voltage' + '\t' + 'voltage_err' + '\t' + 'Current' + '\t' + 'Current_err' + '\n' )

        try:
            print("  Voltage Ramp for IV measurement. If the script stops here, the output is turned off.")

            d = vsource.get_voltage()[0]
            print("  Ramping output from " + str('{:6.2f}'.format(d)) + "V to " + str(vset) + "V")

            # measure loop\
            """
            while( (d-vstep) >= vset ): # Ramping down
                try:
                    vsource.set_voltage(d-vstep, unit='V')
                    time.sleep(twait)
                    d = vsource.get_voltage()[0]
                    current = vsource.get_current()
                    runtime = time.time() - startTime
                    print( "  Running for " + str('{:6.2f}'.format(runtime)) + "s, voltage " + str('{:6.2f}'.format(d)) + " V, measured current " + str('{:.5f}'.format(current[0]))  + " uA ")
                    #3log.write( str('{:6.2f}'.format(runtime)) + " " + str(d) + " " + str(current[0]) + " " + str(current[1]) + "\n" )
                except ValueError:
                    print("  Error occured, chfrom datetime import datetimeeck comliance and voltage.")
                    vsource.set_voltage(   0,unit='V')
                    raise ValueError("Voltage ramp failed")
                except KeyboardInterrupt:
                    vsource.vramp(vstart, vstep, 'V')
                    log.close()
                    break
            """
            while( (d+vstep) <= vset ): # Ramping up
                try:
                    vsource.set_voltage(d+vstep, unit='V')
                    print( "  Waiting " + str(twait) + "s after new voltage")
                    time.sleep(twait)
                    averageVoltage = []
                    averageCurrent = []
                    for x in range(n_Points):
                        d = vsource.get_voltage()[0]
                        current = vsource.get_current()
                        averageVoltage.append(d)
                        averageCurrent.append(current)
                        runtime = time.time() - startTime
                        print( "  Running for " + str('{:6.2f}'.format(runtime)+ '\t') + "s, voltage " + str('{:6.2f}'.format(d)) + " V, measured current " + str('{:.5f}'.format(current[0]))  + " uA ")
                        ##log.write( str('{:6.2f}'.format(runtime)) + " " + str(d) + " " + str(current[0]) + " " + str(current[1]) + "\n" )
                    print("average Voltage = " + str(np.average(averageVoltage)))
                    print("average Current = " + str(np.average(averageCurrent)))
                    now = datetime.now()
                    f.write( now.strftime("%H:%M:%S") + '\t' + str(np.average(averageVoltage)) + '\t' + str(np.std(averageVoltage)) + '\t' + str(np.average(averageCurrent)) + '\t' + '\t' + str(np.std(averageCurrent)) + '\n' )

                    xp.append(float(np.average(averageVoltage)))
                    yp.append(float(np.average(averageCurrent)))


                    ln, = plt.plot(xp, yp, 'b-')
                    ln.set_data(xp, yp)
                    plt.draw()
                    plt.pause(0.001)
                    before, after = str('{:05.2f}'.format(d)).split('.')
                    print("Storing " + str(n_Frames) + " Frames in :" + Frames_Filename + before + "V" + after + ".bin")
                    device.storeFrames(n_Frames, Frames_Filename + before + "V" + after + ".bin")
                except ValueError:
                    print("  Error occured, check comliance and voltage.")
                    vsource.set_voltage(   0,unit='V')
                    raise ValueError("Voltage ramp failed")
                except KeyboardInterrupt:
                    vsource.vramp(vstart, vstep, 'V')
                    log.close()
                    break

        except ValueError:
            exit()

        f.close()
        vsource.disable_output()



        print("wait 2 sec before closing")
        time.sleep(2)

        device.power_off()

        fig.savefig(Frames_Filename + '.pdf')

if __name__ == '__main__':
    import sys
    main(*sys.argv[1:])
