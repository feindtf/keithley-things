# H. Wennlöf, 2018
# F. Feindt,  2021


import yaml
import time
import sys
import matplotlib.pyplot as plt

from Keithley import KeithleySMU2400Series


#############################################
# Simple program to set and monitore voltages
#
# Note:
# Before any settings are changed, the data acquisition needs to be paused
# (as we can't have two processes writing to the # Keithley at the same time).
# Pausing can be done by calling pauseAcq(), unpausing by calling resumeAcq().


# open configuration file
confFileName = sys.argv[1]
print(confFileName)
configuration_file = ''
with open(confFileName, 'r') as file:
    configuration_file = yaml.load(file, Loader=yaml.SafeLoader)

    
# connect to Keithley
vsource = KeithleySMU2400Series(configuration_file)
print("Keithley Initialised.\n")

# build outfile name
logname = configuration_file["OutfileName"]
startTime = time.time()
logFileName = logname + str(startTime) + ".log"
log = open(logFileName, "a")
log.write( "time_since_start[s] voltage [V] current_value[ua] current_error[ua]\n" )


# voltage from config file
vstart = configuration_file["SetVoltage"]["V_Start"]
vstep  = configuration_file["SetVoltage"]["V_Step"]
vset   = configuration_file["SetVoltage"]["V_Set"]
twait  = configuration_file["SetVoltage"]["t_Wait"]

try:
    # set V_Start
    vsource.disable_output()
    vsource.set_voltage(vstart, unit='V')
    vsource.enable_output()
    
except ValueError:
    exit()

# take first measurement
d = vsource.get_voltage()[0]
current = vsource.get_current()
runtime = time.time() - startTime
print( "  Running for " + str(runtime) + "s, voltage " + str(d) + " V, measured current " + str(current[0]) + " uA ")
log.write( str(runtime) + " " + str(d) + " " + str(current[0]) + " " + str(current[1]) + "\n" )
            
try:    
    print("Voltage Ramp for IV measurement. If the script stops here, the output is turned off.")
    d = vsource.get_voltage()[0]
    print("  Ramping output from " + str(d) + "V to " + str(vset) + "V")

    # measure loop
    while( (d-vstep) >= vset ): # Ramping down
        try:
            vsource.set_voltage(d-vstep, unit='V')
            time.sleep(twait)
            d = vsource.get_voltage()[0]
            current = vsource.get_current()
            runtime = time.time() - startTime
            print( "  Running for " + str(runtime) + "s, voltage " + str(d) + " V, measured current " + str(current[0]) + " uA ")
            log.write( str(runtime) + " " + str(d) + " " + str(current[0]) + " " + str(current[1]) + "\n" )
        except ValueError:
            print("  Error occured, check comliance and voltage.")
            vsource.set_voltage(   0,unit='V')
            raise ValueError("Voltage ramp failed")    
        except KeyboardInterrupt:
            vsource.vramp(vstart, vstep, 'V')
            log.close()
            break

    while( (d+vstep) <= vset ): # Ramping up
        try:
            vsource.set_voltage(d+vstep, unit='V')
            time.sleep(twait)        
            d = vsource.get_voltage()[0]
            current = vsource.get_current()
            runtime = time.time() - startTime
            print( "  Running for " + str(runtime) + "s, voltage " + str(d) + " V, measured current " + str(current[0]) + " uA ")
            log.write( str(runtime) + " " + str(d) + " " + str(current[0]) + " " + str(current[1]) + "\n" )
        except ValueError:
            print("  Error occured, check comliance and voltage.")
            vsource.set_voltage(   0,unit='V')
            raise ValueError("Voltage ramp failed")
        except KeyboardInterrupt:
            vsource.vramp(vstart, vstep, 'V')
            log.close()
            break

except ValueError:
    exit()
